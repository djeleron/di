var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var include = require('gulp-include');
var importify = require('gulp-importify');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function () {
    return gulp
        .src([
            '!./**/_*.{scss,sass}',
            '!./**/_*.css',
            './scss/main.{scss,sass}',
        ], {base: process.cwd()})
        .pipe(importify('style.scss', {
            cssPreproc: 'scss'
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./css/'));
});



gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', [
        'sass'
    ]);
});


gulp.task('default', [
    'sass',
    'sass:watch'
]);